# Spring Security OAuth Example

- `spring-security-auth-server` - Has the Authorization Server and Web Security Server
- `spring-security-client` - Client Project which uses Custom Authorization Server for SSO.
- `spring-security-client-okta` - Client Project which uses OKTA for SSO.
- `spring-security-client-multiplesso` - Client Project which uses Multiple Apps for Signing.

###**_Port Mapping:_**

|**Port Number** | **Project**|
|--- | ---|
|**1010**| spring-security-auth-server|
|**1011**| spring-security-client|
|**1012**| spring-security-client-okta|
|**1013**| spring-security-client-multiplesso|
