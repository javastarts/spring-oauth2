package com.neeraj.security.oauth2.multiplesso.controller;

import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
public class HomeController {

    @RequestMapping(value = {"/login/local", "/login/okta", "/login/github"})
    @ResponseBody
    public String sendLoginResponse(@RequestParam("code") String code,
                                    @RequestParam("state") String state,
                                    Principal principal) {
        return user(principal).toString() + " code: " + code + " state:" + state;
    }

    @RequestMapping(value = {"/securedPage"})
    public String goToSecuredPage(@PathVariable String provider, Principal principal) {
        System.out.println("principal: " + principal);
        System.out.println("provider : " + provider);
        OAuth2AuthenticationToken token = (OAuth2AuthenticationToken) principal;
        Map<String, Object> user = token.getPrincipal().getAttributes();
        System.out.println("Below are the User Details:\n" + user);
        return "securedPage";
    }

    @RequestMapping("/unauthenticated")
    @ResponseBody
    public String unauthenticated() {
        return "There was an error.";
    }

    @RequestMapping({"/user", "/me"})
    @ResponseBody
    public Map<String, String> user(Principal principal) {

        OAuth2Authentication auth = (OAuth2Authentication) principal;

        System.out.println("Principal : " + principal);
        System.out.println("OAuth2Authentication : " + auth);
        System.out.println(auth.getUserAuthentication().getDetails());

        Map<String, String> map = new LinkedHashMap<>();
        map.put("name", principal.getName());
        return map;
    }
}
