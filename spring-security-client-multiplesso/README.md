

###Issues:
**Unable to get EnableOauth2Sso Working — BadCredentialsException: Could not obtain access token**

**_`This error was because of both Client and Server was running in localhost, both Apps interfere with each other since both are setting JSESSIONID.`_**

src : https://spring.io/guides/tutorials/spring-boot-oauth2/#_social_login_authserver

There are two easy workarounds:

1. Use server.context-path to move each App to different paths, note that you need to do this for both
2. Set the server.session.cookie.name for one App to something different, e.g., APPSESSIONID
I would suggest to put this workaround in a profile that you activate for localhost only.