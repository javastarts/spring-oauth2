package com.neeraj.security.oauth.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping(value = {"/index", "/"})
    public String goToHomePage() {
        return "index";
    }

    @RequestMapping(value = "/securedPage")
    public String goToSecuredPage() {
        return "securedPage";
    }
}
