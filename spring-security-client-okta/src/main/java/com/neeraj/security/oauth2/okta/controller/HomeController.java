package com.neeraj.security.oauth2.okta.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.Map;

@Controller
public class HomeController {

    @Autowired
    private OAuth2AuthorizedClientService authorizedClientService;

    @RequestMapping(value = {"/index", "/"})
    public String goToHomePage(Model model, OAuth2AuthenticationToken authentication) {
        if (authentication != null) {
            OAuth2AuthorizedClient authorizedClient = this.getAuthorizedClient(authentication);
            model.addAttribute("userName", authentication.getName());
            model.addAttribute("clientName", authorizedClient.getClientRegistration().getClientName());
        }
        return "index";
    }

    @RequestMapping(value = "/securedPage")
    public String goToSecuredPage(Principal principal) {
        System.out.println("principal: " + principal);
        OAuth2AuthenticationToken token = (OAuth2AuthenticationToken) principal;
        System.out.println("Authorised by: " + token.getAuthorizedClientRegistrationId());
        Map<String, Object> user = token.getPrincipal().getAttributes();
        System.out.println("Below are the User Details:\n" + user);
        return "securedPage";
    }

    private OAuth2AuthorizedClient getAuthorizedClient(OAuth2AuthenticationToken authentication) {
        return this.authorizedClientService.loadAuthorizedClient(
                authentication.getAuthorizedClientRegistrationId(), authentication.getName());
    }

//    private ExchangeFilterFunction oauth2Credentials(OAuth2AuthorizedClient authorizedClient) {
//        return ExchangeFilterFunction.ofRequestProcessor(
//                clientRequest -> {
//                    ClientRequest authorizedRequest = ClientRequest.from(clientRequest)
//                            .header(HttpHeaders.AUTHORIZATION,
//                                    "Bearer " + authorizedClient.getAccessToken().getTokenValue())
//                            .build();
//                    return Mono.just(authorizedRequest);
//                });
//    }
}